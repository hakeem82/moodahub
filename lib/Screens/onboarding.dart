import 'package:flutter/material.dart';
import 'package:pressfame_new/Screens/login.dart';
import 'package:pressfame_new/constant/global.dart';
import 'package:pressfame_new/helper/sizeConfig.dart';
import 'package:pressfame_new/models/sliderModel.dart';

class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  List<SliderModel> slides = new List<SliderModel>();
  int currentIndex = 0;
  PageController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = PageController(initialPage: 0);
    slides = getSlides();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: PageView.builder(
                scrollDirection: Axis.horizontal,
                onPageChanged: (value) {
                  setState(() {
                    currentIndex = value;
                  });
                },
                itemCount: slides.length,
                itemBuilder: (context, index) {
                  // contents of slider
                  return Slider(
                    image: slides[index].getImage(),
                    text: slides[index].getText(),
                  );
                }),
          ),

          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(
                slides.length,
                (index) => buildDot(index, context),
              ),
            ),
          ),
          Container(
            child: _signupButton(context),
            width: double.infinity,
             /*FlatButton(
              child:
                  Text(currentIndex == slides.length - 1 ? "Continue" : "Next"),
              onPressed: () {
                if (currentIndex == slides.length - 1) {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => Login()),
                  );
                }
                _controller.nextPage(
                    duration: Duration(milliseconds: 100),
                    curve: Curves.bounceIn);
              },
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25),
              ),
            ),*/
          ),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

// container created for dots
  Container buildDot(int index, BuildContext context) {
    return Container(
      height: 10,
      width: currentIndex == index ? 25 : 10,
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Color(0xFF200e32),
      ),
    );
  }
  Widget _signupButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Padding(
        padding: const EdgeInsets.only(right: 20, top: 0, left: 20),
        child: SizedBox(
          height: SizeConfig.blockSizeVertical,
          width: SizeConfig.screenWidth,
          child: CustomButtom(
            title: 'Sign-Up',
            color: Color(0xFF200e32),
            onPressed: () {
              /*if (emailController.text != '' && passwordController.text != '') {
                setState(() {
                  emailNode.unfocus();
                  passwordNode.unfocus();
                  isLoading = true;
                });

                _signInWithEmailAndPassword();
              } else {
                setState(() {
                  emailNode.unfocus();
                  passwordNode.unfocus();
                });
                toast("Error", "Email and password is required", context);
              }*/
            },
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
// slider declared
class Slider extends StatelessWidget {
  String image;
  String text;

  Slider({this.image, this.text});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      // contains container
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // image given in slider
            Container(
              decoration:
                  BoxDecoration(
                      border: Border.all(
                          color: Colors.black,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))
                  ),
             child: Padding(
              padding: EdgeInsets.all(50.0),
              child: Image(image: AssetImage(image)),
             ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.all(50.0),
                child: Text(
                  text,
                  style: TextStyle(
                  color: Colors.black,
                  fontSize: 16),
                ),
              ),
            ),
            SizedBox(height: 25),
          ],
        ),
      ),
    );
  }
}
