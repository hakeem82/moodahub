class SliderModel{
  String image;
  String text;

// images given
  SliderModel({this.image, this.text});

// setter for image
  void setImage(String getImage){
    image = getImage;
  }

  // setter for slider text
  void setText(String sliderText){
    text = sliderText;
  }


// getter for image
  String getImage(){
    return image;
  }


// getter for slider text
  String getText(){
    return text;
  }
}
List<SliderModel> getSlides(){
  List<SliderModel> slides = new List<SliderModel>();
  SliderModel sliderModel = new SliderModel();

// 1
  sliderModel.setImage("assets/images/onboarding/onboarding_01.png");
  sliderModel.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

// 2
  sliderModel.setImage("assets/images/onboarding/onboarding_01.png");
  sliderModel.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.");

  slides.add(sliderModel);

  sliderModel = new SliderModel();

// 3
  sliderModel.setImage("assets/images/onboarding/onboarding_01.png");
  sliderModel.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.");

  slides.add(sliderModel);

  sliderModel = new SliderModel();
  return slides;
}
